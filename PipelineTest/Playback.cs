﻿using Gst;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PipelineTest
{
    class Playback
    {
        static GLib.MainLoop mainLoop;

        static void Run(object o)
        {
            int i = 500;
            while (i-- > 0)
            {
                //Pipeline pipeline = (Pipeline)Parse.Launch("uridecodebin name=uridecodebin ! videoconvert ! textoverlay name=textoverlay ! fakesink name=vsink");
                Pipeline pipeline = (Pipeline)Parse.Launch("videotestsrc ! fakesink");

                //var uridecodebin = pipeline.GetByName("uridecodebin");
                // uridecodebin["uri"] = "rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov";

                pipeline.SetState(State.Playing);

                bool terminated = false;
                do
                {
                    var msg = pipeline.Bus.PopFiltered(MessageType.StateChanged);
                    if (msg == null) continue;
                    msg.ParseStateChanged(out State oldstate, out State newstate, out State pendingstate);

                    if (newstate == State.Playing) terminated = true;
                    Console.WriteLine("State {0} to {1} - pending {2}", 
                        Element.StateGetName(oldstate),
                        Element.StateGetName(newstate),
                        Element.StateGetName(pendingstate));
                    msg.Dispose();
                } while (!terminated);

                Thread.Sleep(3000);
                pipeline.SetState(State.Null);

                pipeline.Bus.Dispose();
                pipeline.Dispose();

            }

            mainLoop.Quit();
        }

        public static void Main(string[] args)
        {
            Environment.SetEnvironmentVariable("GST_DEBUG", "GST_TRACER:7");
            Environment.SetEnvironmentVariable("GST_TRACERS", "leaks");
            Application.Init(ref args);

            GLib.MainContext context = new GLib.MainContext();
            mainLoop = new GLib.MainLoop(context);


            Thread thread = new Thread(Run);
            thread.Start();

            mainLoop.Run();

            thread.Join();
            Console.ReadLine();
        }
    }
}
